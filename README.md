# GroupProject1

## C Programming Temple University CIS 1057
For Group Project #1, you want to edit the files in this source code repository to match the three specific functions you've been assigned.

Each group member should be responsible for completing one of the specific functions.

Make sure your function comments are correct -- they will count for a significant part of your grade on this project.

If you have any questions, email labwork@temple.edu.


-----
