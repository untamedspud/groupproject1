#include <stdio.h>
#include <stdlib.h>
#include "group1.h"

int funct2( int n )
{
	printf( 
		"STUB!\n"
		"    In function funct2 with parameter n = %d.\n"
		"    Returning the value EXIT_SUCCESS in stub.\n",
			n );

	return EXIT_SUCCESS;
}
