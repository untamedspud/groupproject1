#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "group1.h"

char *funct3( char *n )
{
	printf( 
		"STUB!\n"
		"    In function funct3 with parameter n = \"%s\".\n"
		"    Returning the value passed in to the stub.\n",
			n );

	return n;
}