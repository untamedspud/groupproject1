#ifndef GROUP1_H
#define GROUP1_H

#define GROUP_MEMBERS  "Group Member 1, Group Member 2, Group Member 3"

#define MY_ERROR_1     "There has been an error: the string is wrong."
#define USE_DAY        "Monday"
#define HAPPINESS      "Is a warm puppy."

// function prototypes for this project
int   funct1( int );	// calculates PI to n digits and returns the value
int   funct2( int );	// reads in a number and squares it
char *funct3( char * );	// converts all the T's in a string to 'U'

#define FUNCTION_A_DESCRIPTION  "Calculates PI to n digits and returns the value"
#define FUNCTION_B_DESCRIPTION  "Reads in a number and squares it"
#define FUNCTION_C_DESCRIPTION  "Converts all the T's in a string to 'U'"

#endif
