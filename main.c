#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "group1.h"


/*
 * Author:      Group Number nnn
 * Facility:    Temple University Computer Science 
 * Class:       C Programming 1057 Fall 2024 Section 004 Course 29881
 * Date:        Monday, September 30, 2024
 * Assignment:  Driver program for our three functions.
 * Description: Test our three function for the group.
 */

/* Prototypes for our driver functions in this main.c file. */
void program_identification_group_project( void );
void driver_function_1( void );
void driver_function_2( void );
void driver_function_3( void );


int main()
{
	program_identification_group_project( );

	puts( "Invoking driver #1." );
	driver_function_1( );
	puts( "Driver 1 is complete.\n\n");

	puts( "Invoking driver #2." );
	driver_function_2( );
	puts( "Driver 1 is complete.\n\n");

	puts( "Invoking driver #3." );
	driver_function_3( );
	puts( "Driver 1 is complete.\n\n");

	puts( "Driver program complete." );

	return EXIT_SUCCESS;
}



/*
 * Function:          program_identification_group_project
 * Preconditoons:     None.
 * Postconditions:    None.
 * Returns:           Nothing.
 * Globals:           GROUP_MEMBERS - the members of our group.
                      FUNCTION_A_DESCRIPTION - what our first function does.
					  FUNCTION_B_DESCRIPTION - what our second function does.
					  FUNCTION_C_DESCRIPTION - what our third function does.
					      Note: Our globals are in the 'group1.h' header file.
 * Description:       Displays identifying information on our group
                      members and what our three functions do.
 */
void program_identification_group_project( void )
{
	puts( "================" );
	puts( "Group Project #1" );
	puts( "================" );
	printf( "Group members: %s.\n\n", GROUP_MEMBERS );

	puts( "Function A: " FUNCTION_A_DESCRIPTION );	
	puts( "Function B: " FUNCTION_B_DESCRIPTION );
	puts( "Function C: " FUNCTION_C_DESCRIPTION );

	puts( "================\n\n\n" );

	return;
}

void driver_function_1( )
{
	// calculates PI to n digits and returns the value
	puts( "<> Invoking function1 as funct1( 4 )." );
	int rc = funct1( 4 );
	printf( "<> funct1( 4 ) returned the value %d.\n", rc );

	return;
}

void driver_function_2( )
{
	// reads in a number and squares it
	int valin = 7;
	printf( "<> Invoking function2 as funct2( %d ).\n", valin );
	int rc = funct2( valin );
	printf( "<> funct2( %d ) returned the value %d.\n", valin, rc );

	return;
}

void driver_function_3( )
{
	// converts all the T's in a string to U
	char *mystring = "There are Ts in this string.";
	printf( "<> Invoking function3 as funct3( %s ).\n", mystring );
	char *rc = funct3( mystring );
	printf( "<> funct3( %s ) returned the value \"%s\".\n", mystring, rc );

	return;
}
